package com.training.resourcemanagment.exceptions;

import com.training.resourcemanagment.models.ErrorMessage;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class ServerNotExistMapper implements ExceptionMapper<ServerNotExist> {
    @Override
    public Response toResponse(ServerNotExist exception) {
        ErrorMessage errorMessage = new ErrorMessage(exception.getMessage());
        return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
    }
}
