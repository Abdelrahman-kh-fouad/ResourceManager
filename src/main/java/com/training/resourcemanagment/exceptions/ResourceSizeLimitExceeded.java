package com.training.resourcemanagment.exceptions;

import jakarta.ws.rs.Produces;

public class ResourceSizeLimitExceeded extends Exception {
    public ResourceSizeLimitExceeded() {
        super("Maximum allowed resource size is 100GB");
    }
}
