package com.training.resourcemanagment.exceptions;

import com.training.resourcemanagment.services.ServerRepo;

public class ServerNotExist extends Exception{
    public ServerNotExist() {
        super("There arn't any servers.");
    }
}
