package com.training.resourcemanagment.exceptions;

import com.training.resourcemanagment.models.ErrorMessage;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class ResourceSizeLimitExceededMapper implements ExceptionMapper<ResourceSizeLimitExceeded> {
    @Override
    public Response toResponse(ResourceSizeLimitExceeded exception) {
        ErrorMessage errorMessage = new ErrorMessage(exception.getMessage());
        return Response.status(Response.Status.NOT_ACCEPTABLE).entity(errorMessage).build();
    }
}
