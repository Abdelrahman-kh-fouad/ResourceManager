package com.training.resourcemanagment.controller;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Application;
@ApplicationPath("/api")
public class RESTApplication extends Application {

}
