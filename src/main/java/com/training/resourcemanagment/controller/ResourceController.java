package com.training.resourcemanagment.controller;

import com.training.resourcemanagment.exceptions.ResourceSizeLimitExceeded;
import com.training.resourcemanagment.exceptions.ServerNotExist;
import com.training.resourcemanagment.models.dao.Server;
import com.training.resourcemanagment.models.mappers.ServerMapper;
import com.training.resourcemanagment.services.ResourcesServices;
import com.training.resourcemanagment.services.ResourcesServicesImpl;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Resources Controller: controlling all Resources as add request or get all serveces.
 */
@Path("/resource")
public class ResourceController {
    /**
     * @param size the size of resource.
     * @return server about.
     * @throws ResourceSizeLimitExceeded: checking for resource limitation.
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response askForResource(@FormParam("size") int size) throws ResourceSizeLimitExceeded, InterruptedException {
        ResourcesServices resourcesServices = new ResourcesServicesImpl();
        Server result = resourcesServices.addResource(size);
        return Response.status(Response.Status.ACCEPTED)
                .entity(ServerMapper.INSTANCE.resourceDaoToResourceDto(result)).build();
    }

    /**
     * @return get all servers.
     * @throws ServerNotExist: check id there is any server.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllResources() throws ServerNotExist {
        ResourcesServices resourcesServices = new ResourcesServicesImpl();
        return Response.status(Response.Status.ACCEPTED)
                .entity(ServerMapper.INSTANCE.resourceDaoToResourceDto(resourcesServices
                .getservers())).build();
    }
}
