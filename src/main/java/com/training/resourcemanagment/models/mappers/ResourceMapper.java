package com.training.resourcemanagment.models.mappers;

import com.training.resourcemanagment.models.dao.Resource;
import com.training.resourcemanagment.models.dto.ResourceDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ResourceMapper {
    ResourceMapper INSTANCE = Mappers.getMapper(ResourceMapper.class);
    ResourceDto resourceDaoToResourceDto(Resource resource);
    List<ResourceDto> resourceDaoToResourceDto(List<Resource> resourceList);
}