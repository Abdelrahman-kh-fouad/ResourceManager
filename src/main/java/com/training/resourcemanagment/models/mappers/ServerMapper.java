package com.training.resourcemanagment.models.mappers;

import com.training.resourcemanagment.models.dao.Server;
import com.training.resourcemanagment.models.dto.ServerDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ServerMapper {
    ServerMapper INSTANCE = Mappers.getMapper(ServerMapper.class);
    ServerDto resourceDaoToResourceDto(Server server);
    List<ServerDto> resourceDaoToResourceDto(List<Server> serverList);
}
