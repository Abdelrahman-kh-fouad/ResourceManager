package com.training.resourcemanagment.models.dto;

import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.List;

@XmlRootElement
public class ServerDto {
    private String creationTime;
    private int id;
    private List<ResourceDto> resourceList;
    public ServerDto(){}

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<ResourceDto> getResourceList() {
        return resourceList;
    }

    public void setResourceList(List<ResourceDto> resourceList) {
        this.resourceList = resourceList;
    }

    @Override
    public String toString() {
        return "ServerDTO{" +
                "creationTime='" + creationTime + '\'' +
                ", id=" + id +
                ", resourceList=" + resourceList +
                '}';
    }
}
