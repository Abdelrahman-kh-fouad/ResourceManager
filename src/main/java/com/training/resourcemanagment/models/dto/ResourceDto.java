package com.training.resourcemanagment.models.dto;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class ResourceDto {
    private String creationTime;
    private long id;
    private int size;
    public ResourceDto(){}

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "ResourceDTO{" +
                "creationTime='" + creationTime + '\'' +
                ", id=" + id +
                ", size=" + size +
                '}';
    }
}
