package com.training.resourcemanagment.models.dao;

import java.time.LocalDateTime;

/**
 * Resource class (the actual resource)
 */
public class Resource {
    static private long idCounter = 0;
    private long id;
    private LocalDateTime creationTime;
    private int size;
    public Resource(){}
    public Resource(int size) {
        this.id = idCounter++;
        this.creationTime = LocalDateTime.now();
        this.size = size;
    }

    public static long getIdCounter() {
        return idCounter;
    }

    public long getId() {
        return id;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "Resource{" +
                "id=" + id +
                ", creationTime=" + creationTime +
                ", size=" + size +
                '}';
    }
}
