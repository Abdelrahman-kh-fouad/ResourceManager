package com.training.resourcemanagment.models.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Server of resources, we could imaging it as a fragment
 */
public class Server implements Comparable<Server> {
    public static final int MAX_SIZE = 100;
    private static Long idCounter = null;
    private  long id;
    private int size;
    private State state;
    private LocalDateTime creationTime;
    private List<Resource> resourceList;

    public Server(Resource resource) {
        if (idCounter == null) {
            idCounter = new Long(0);
        }
        this.state = State.STARTING;
        try {
            Thread.sleep(9600);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        synchronized (this) {
            this.id = idCounter++;
        }
        this.size = 0;
        this.resourceList = new ArrayList<>() {};
        this.creationTime = LocalDateTime.now();
        this.state = State.ACTIVE;
        if (resource.getSize() != 0) {
            this.addResource(resource);
        }
    }
    public void addResource(Resource resource) {
        this.resourceList.add(resource);
        this.size += resource.getSize();
    }
    public int getFreeSize() {
        return MAX_SIZE - size;
    }


    public long getId() {
        return id;
    }

    public State getState() {
        return state;
    }

    public synchronized void setState(State state) {
        this.state = state;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public List<Resource> getResourceList() {
        return resourceList;
    }

    public int getSize() {return size;}

    @Override
    public int compareTo(Server o) {
        if(o.getFreeSize() > this.getFreeSize()) {
            return 1;
        } else if (o.getFreeSize() < this.getFreeSize()) {
            return -1;
        } else {

            if(o.getCreationTime().isBefore(this.getCreationTime())) {
                return 1;
            } else if(o.getCreationTime().isAfter(this.getCreationTime())) {
                return -1;
            }
            return 0;

        }
    }

    @Override
    public String toString() {
        return "Server{" +
                "id=" + id +
                ", size=" + size +
                ", creationTime=" + creationTime +
                ", resourceList=" + resourceList +
                '}';
    }
}
