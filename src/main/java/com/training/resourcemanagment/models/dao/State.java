package com.training.resourcemanagment.models.dao;

public enum State {
    READY_TO_CHANGE, ACTIVE, STARTING;
}
