package com.training.resourcemanagment.services;

import com.training.resourcemanagment.models.dao.Resource;
import com.training.resourcemanagment.models.dao.Server;
import com.training.resourcemanagment.models.dao.State;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.locks.*;

/**
 * ServerRepo is most important class, it contains best-fit and locks safety.
 */
public class ServerRepo {
    private static ServerRepo INSTANCE = null;
    private Set<Server> serverSet;
    ReadWriteLock readWriteLock;

    private ServerRepo() {
        this.serverSet = new TreeSet();
        readWriteLock = new ReentrantReadWriteLock();
    }

    /**
     * @param resource resource you want to allocate.
     * @return return the server where we allocate the resource(it could be new one, or exist one)
     */
    public Server getBestFitServer(Resource resource) {
        Server currentServer = null;
        readWriteLock.writeLock().lock();
        for(Server it: serverSet) {
            if(resource.getSize() <= it.getFreeSize() && it.getState() != State.READY_TO_CHANGE) {
                currentServer = it;
            }
        }
        if(currentServer != null) {
            currentServer.setState(State.READY_TO_CHANGE);
        }
        readWriteLock.writeLock().unlock();
        if(currentServer != null) {
            currentServer = addResource(currentServer, resource);
            currentServer.setState(State.ACTIVE);
        } else {
            currentServer = createServer(resource);
        }
        return currentServer;
    }

    /**
     * Editing the hole server set with locks.
     * @param server server where you want to allocate your resource.
     * @param resource resource you want to allocate.
     * @return server itself.
     */
    private Server addResource(Server server, Resource resource) {
        readWriteLock.writeLock().lock(); //only one thread con modifiy the set.
        this.serverSet.remove(server);
        server.addResource(resource);
        this.serverSet.add(server);
        readWriteLock.writeLock().unlock();
        return server;
    }

    /**
     * @param resource resource to allocate after starting the server.
     * @return the server itself.
     */
    private Server createServer(Resource resource) {
        Server newServer = new Server(new Resource(0));
        addResource(newServer, resource);
        return newServer;
    }

    /**
     * @return All servers as a list.
     */
    public List<Server> getServerList() {
        ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        readWriteLock.readLock().lock();
        // make a copy, Read lock(many thread could reach at the same time).
        List<Server> serverList = serverSet.stream().parallel().toList();
        readWriteLock.readLock().unlock();
        return serverList;
    }

    public void reset() {
        serverSet = new TreeSet<>();
    }
    public static synchronized ServerRepo getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ServerRepo();
        }
        return INSTANCE;
    }
}
