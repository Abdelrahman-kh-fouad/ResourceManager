package com.training.resourcemanagment.services;

import com.training.resourcemanagment.exceptions.ResourceSizeLimitExceeded;
import com.training.resourcemanagment.exceptions.ServerNotExist;
import com.training.resourcemanagment.models.dao.Resource;
import com.training.resourcemanagment.models.dao.Server;

import java.util.List;

public class ResourcesServicesImpl implements ResourcesServices {

    public ResourcesServicesImpl() {}

    @Override
    public Server addResource(int size) throws ResourceSizeLimitExceeded, InterruptedException {
        if(size <= 100) {
            Resource resourceToAdd = new Resource(size);
            return ServerRepo.getInstance().getBestFitServer(resourceToAdd);
        } else {
            throw new ResourceSizeLimitExceeded();
        }
    }

    @Override
    public List<Server> getservers() throws ServerNotExist {
        List<Server> serverList = ServerRepo.getInstance().getServerList();
        if(serverList.size() == 0) {
            throw new ServerNotExist();
        } else {
            return serverList;
        }
    }
}
