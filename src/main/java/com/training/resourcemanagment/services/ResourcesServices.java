package com.training.resourcemanagment.services;

import com.training.resourcemanagment.exceptions.ResourceSizeLimitExceeded;
import com.training.resourcemanagment.exceptions.ServerNotExist;
import com.training.resourcemanagment.models.dao.Server;

import java.util.List;

public interface ResourcesServices {
    public Server addResource(int size) throws ResourceSizeLimitExceeded, InterruptedException;
    public List<Server> getservers() throws ServerNotExist;
}
