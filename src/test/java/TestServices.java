import com.training.resourcemanagment.exceptions.ResourceSizeLimitExceeded;
import com.training.resourcemanagment.exceptions.ServerNotExist;
import com.training.resourcemanagment.models.dao.Resource;
import com.training.resourcemanagment.models.dao.Server;
import com.training.resourcemanagment.services.ResourcesServices;
import com.training.resourcemanagment.services.ResourcesServicesImpl;
import com.training.resourcemanagment.services.ServerRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestServices {

    static ResourcesServices resourcesServices ;
    @BeforeAll
    static void setUp() {
        resourcesServices = new ResourcesServicesImpl();
        ServerRepo.getInstance().reset();
    }

    @Test
    @Order(1)
    void getAllServers_serversNotFound() {
        assertThrowsExactly(ServerNotExist.class, () ->
                        resourcesServices.getservers(),
                "Should assert server doesn't exest");
    }
    @Test
    @Order(2)
    void AddResource_withNewServer_correctSize() {
        assertDoesNotThrow(() ->
                resourcesServices.addResource(12),
                        "Should accept this size");

    }
    @Test
    @Order(3)
    void AddResource_withNewServer_UncorrectSize() {
        assertThrowsExactly(ResourceSizeLimitExceeded.class, () ->
                resourcesServices.addResource(120),
                "Should'nt accept this size");

    }
    @Test
    @Order(4)
    void getAllServers_serversExists() {
        assertDoesNotThrow(() ->
                assertTrue(resourcesServices.getservers().size() > 0,
                        "Should return Servers"));
    }
    @Test
    @Order(5)
    void addResources_toTheSameServer() {
        ServerRepo.getInstance().getBestFitServer(new Resource(18));
        ServerRepo.getInstance().getBestFitServer(new Resource(35));
        ServerRepo.getInstance().getBestFitServer(new Resource(35));
        assertTrue(ServerRepo.getInstance().getServerList().size() == 1);
    }
    @Test
    @Order(6)
    void bestFit_testing() {
        // there is Server with 100GB as one server of  resources
        long id = ServerRepo.getInstance().getBestFitServer(new Resource(90)).getId();
        ServerRepo.getInstance().getBestFitServer(new Resource(40));
        ServerRepo.getInstance().getBestFitServer(new Resource(62));

        Server newone = ServerRepo.getInstance().getBestFitServer(new Resource(10));
        assert (id == newone.getId());
    }
    @Test
    @Order(7)
    void reset_testing() {
        ServerRepo.getInstance().reset();
        assertThrowsExactly(ServerNotExist.class, () ->
                        resourcesServices.getservers(),
                "Should assert server doesn't exest");
    }

}
